public class Principal{
public static void main (String[] args) {//declaracion del main
Operaciones operaciones =new Operaciones();//aqui se instancia la clase Operaciones
operaciones.saludo();//Se llama al saludo
operaciones.estado(76);//Se llama al metodo estado el estado y le da la nota 76 como parametro
operaciones.suma(50,25);//Se llama al metodo suma , dandole dos numetos de parametro 50,25
operaciones.listaNumero(5);///Se llama metodo listaNumero y le da el 5 como parametro
operaciones.restaMultiplicacion(20.2,3.4,2.5);///llama el metodo restaMultiplicacion y le da 3 valores de tipo decimal como parametro
}
public static class Operaciones
{//Se crea la clase Operaciones
public void saludo(){//se crea el metodo1 saludo , sin parametro para devolver un mensaje
System.out.println("Soy el mejor programador del mundo ");//mensaje
}
public void estado(int nota)
{//se crea el metodo estado el cual recibe como parametro la variable de tipo entero
if (nota>=70){//se hace una comparacion , si es mayor o igual a 70
System.out.println("Aprobado");//da mensaje Aprobado
}else{//si no
System.out.println("Reprobado");//da como //mensajeReprobado
}
}
public int suma(int a,int b){///metodo suma recibe dos parametros de tipo entero
int suma=a+b;//se declara la variable suma , y se hace la suma de los dos numeros de parametroo
System.out.println("La suma es: "+suma);//mensaje de salida de la suma
return suma;//retorna el resultado de la suma
}
public int listaNumero(int n)
{///metodo listaNumero ,recibe in parametro de tipo entero que es el que da el limite
for (int x=1;x<=n ;x++ ) {//se crea un for , para recorrer hasta donde se mostraran los numeros
System.out.println (x+" ");//mensaje que muestra la x , mostrando los numeros
}
return n;//retorna los numeros
}
public void restaMultiplicacion(double n1,double n2,double n3)
{//metodo restaMultiplicacion resive 3 parametros de tipo decimal
double resta=n1-n2-n3;//declaramos la variable resta de tipo decimal , inicializamos dandole la operacion que reste los numeros
double multiplicacion=n1*n2*n3;//declaramos la variabble multi de tipo decimal , inicializamos la operacion
System.out.println("La resta es :"+resta+" La multiplicacion es :"+multiplicacion);//resultado de las operaciones con el mensaje
}
}
}