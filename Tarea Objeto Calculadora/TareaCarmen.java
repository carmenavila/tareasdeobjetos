public class TareaCarmen
{
	
	public static void main (String[] args)
    {
		
	System.out.println("Calculadora 1");
	Casio casio=new Casio();
	System.out.println(casio.Fabricante()+"\n"+casio.Tama�o()+"\n"+casio.Color()+"\n"+casio.Modelo());
		System.out.println(" ");
		
	System.out.println("Calculadora 2");
	Sharp sharp=new Sharp ();
	System.out.println(sharp.Fabricante()+"\n"+sharp.Tama�o()+"\n"+sharp.Color()+"\n"+sharp.Modelo());
	System.out.println(" ");

	StarOffice s=new StarOffice();
	System.out.println("Calculadora 3");
	System.out.println(s.Fabricante()+"\n"+s.Tama�o()+"\n"+s.Color()+"\n"+s.Modelo());
	
	}
	public static abstract class Calculadora
	{
		abstract String Fabricante();
		abstract String Tama�o();
		abstract String Color();
		abstract String Modelo();
		
	}
	public static class Casio extends Calculadora
	{
			public String Fabricante()
		{
			return "Fabricante = Casio";
		}
    	public String Tama�o()
    	{
			return "Tama�o = 10.3cm x 13.7cm";
		}
		public String Color()
		{
			return "Color = Negro";
		}
		public String Modelo()
		{
			return "Modelo = MS-8B";
		}
	
	}
	public static class Sharp extends Calculadora
	{
		public String Fabricante()
		{
			return "Fabricante = Sharp";
		}
    	public String Tama�o()
    	{
			return "Tama�o = 20mm x229mm x122mm";
		}
		public String Color()
		{
			return "Color = Blanco y Azul";
		}
		public String Modelo()
		{
			return "Modelo = EL-240SAB";
		}
		
	}
	public static class StarOffice extends Calculadora
	{
			public String Fabricante()
		{
			return "Fabricante = 5 Star Office";
		}
    	public String Tama�o()
    	{
			return "Tama�o = 9.4cm x 3.2cm ";
		}
		public String Color()
		{
			return "Color = Negro";
		}
		public String Modelo()
		{
			return "Modelo = DT10D";
		}
		
	}
}