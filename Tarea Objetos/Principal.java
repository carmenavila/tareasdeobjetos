public class Principal {///Clase principal
public static void main (String[] args) {//se crea el main
Computadora pc=new Computadora();//se instancia la clase Computadora
pc.setMarca("HP");//se da la marca de la clase Computadora
pc.setEdad(2);//se da la edad de la computadora de la clase Computadora dandole el valor de 2
pc.setColor("Negro");//se da el Color de la computadora
System.out.println("Marca de la computadora= "+pc.getMarca());//se obtiene la marca de la computadora
System.out.println("Edad de la computadora = "+pc.getEdad());//se obtiene la edad de la computadora
System.out.println("El color de la computadora es= "+pc.getColor());//se obtiene elcolor de la computadora
pc.tamano(15);
}
public static class Computadora{///se crea la clase Computadora
private String marca;//se declara la variable nombre como un atributo
private int edad;//se declara la variable edad como un atributo
private String color;//se declara la variable color como un atributo
public String getMarca(){//se crea el metodo para obtener la marca
return marca;//devuelve la marca
}
public int getEdad(){//se crea el metodo para obtener la edad
return edad;//devuelve la edad
}
public String getColor(){//se crea el metodo para obtener el color
return color;//devuelve el color
}
public void setMarca(String marca){//se crea el metodo para dar la marca
this.marca=marca;//se inicialida la variable marca
}
public void setEdad(int edad){//se crea el metodo para dar la edad
this.edad=edad;//se inicializa la variable edad
}
public void setColor(String color){//se crea el metodo para dar el color
this.color=color;//se inicializa la variable color
}
public void tamano(int t){//metodo para dar el tamano de la computadora
if (t<=10){//se compara el tamano si es menor a 10 cm
System.out.println("Tama%o= Mini laptop");//da como resultado Mini laptop
}
if(t>10 && t<20)//si es mayor a 10cm pero es menor a 20cm
{
System.out.println("Tama%o= Mediana");//mensaje mediana
}
else{//caso contario entonces es grande.
System.out.println("Tama%o= Grande");//mensaje grande
}
}
}
}