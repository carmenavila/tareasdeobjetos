
///Superclase
    public  class Flores {
            ///se declaran los atributos
            public String tipo;
            public String color;
            public int tamanio;
            
      public Flores() {
      }
        public Flores(String tipo ,String color,int tamanio) {
        //constructor de la superclase .
        this.tipo= tipo;
        this.color= color;
        this.tamanio= tamanio;
        
      }
      public void tipo(String n) {//El metodo tipo recibe un parametro tipo String
        tipo= n;//tipo va a ser igual a el parametro del metodo
      }
      public void color(String n) {//El metodo color recibe un parametro tipo String
         color= n;///color va a ser igual a el parametro del metodo
       }
      public void tamanio(int n) { //El metodo tamanio recibe un parametro tipo int
         tamanio= n;///tamanio va a ser igual a el parametro del metodo
       }
       public void imprimirInformacion() {///metodo para imprimir el valor de las variable
          System.out.println("El tipo de flor es : " + tipo);//imprime el valor del metodo tipo
          System.out.println("Color: " + color);//imprime el valor del metodo color
          System.out.println("El tamanio maximo es : " + tamanio+"cm");//imprime el valor del metodo tamanio
         
       }
    }