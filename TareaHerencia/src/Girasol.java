///Subclase3
 public  class Girasol extends Flores{//hereda de la clase Flores
       public Girasol() {//costructor de la clase 
          super("Girasol", "Amarillo",40);//Recibe los parametro de la superclase
      }
      public String hojas() {///metodo para dar el color de las hojas
        String color= "Color hojas: Verde oscuro";//declara la variable color, y da valor
        System.out.println(color);///muestra el color de las hojas
        return color;//se retorna el color
      }
       public String espinas() {//metodo para decir si tiene espinas o no
        String tiene="No";//declara variable tiene , y se inicializa
        System.out.println("Espinas en su tallo: "+tiene);//imprime el resultado de la variable tiene
        return tiene;//se retorna la variable tiene
      }
    }