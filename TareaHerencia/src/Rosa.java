///Subclase1
 public  class Rosa extends Flores{//hereda de la clase Flores
       public Rosa() {//costructor de la clase 
          super("Rosa", "Blanco",30);//Recibe los parametro de la superclase
      }
      public String hojas() {///metodo para dar el color de las hojas
        String color= "Color hojas: Verde";//declara la variable color, y da valor
        System.out.println(color);///muestra el color de las hojas
        return color;//se retorna el color
      }
       public String espinas() {//metodo para decir si tiene espinas o no
        String tiene="Si";//declara variable tiene , y se inicializa
        System.out.println("Espinas en su tallo: "+tiene);//imprime el resultado de la variable tiene
        return tiene;//se retorna la variable tiene
      }
    }